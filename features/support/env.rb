require 'capybara/cucumber'


Capybara.register_driver :firefox do |app|
  Capybara::Selenium::Driver.new(app,
                                 :browser => :remote,
                                 :desired_capabilities => :firefox,
                                 :url => "http://webdriver:4444/wd/hub"
                                )
end

Capybara.server_port = 80
Capybara.app_host = "http://foobar.com:#{Capybara.server_port}"

Capybara.default_driver = :firefox
Capybara.javascript_driver = :firefox


