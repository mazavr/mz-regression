When("visit {string}") do |path|
  visit path

end

Then("page content should include {string}") do |s|
  expect(page).to have_content(s)
end

